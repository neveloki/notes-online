import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home/Home.vue';
import Details from '../views/Details/Details.vue';
import Note from '../components/blocks/Note/Note.vue';
import PageNotFound from '../components/blocks/PageNotFound/PageNotFound.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: { title: 'Заметки онлайн' },
  },
  {
    path: '/details',
    name: 'Details',
    component: Details,
    meta: { title: 'Новая заметка' },
    children: [
      {
        path: ':id',
        name: 'Note',
        component: Note,
        meta: { title: 'Редактор заметки' },
      },
    ],
  },
  {
    path: '*',
    name: 'PageNotFound',
    component: PageNotFound,
    meta: { title: 'Страница не найдена' },
  },
];

const router = new VueRouter({
  routes,
  mode: 'history',
});

router.beforeEach((to, from, next) => {
  document.title = to.meta.title || 'Заметки онлайн';
  next();
});

export default router;
