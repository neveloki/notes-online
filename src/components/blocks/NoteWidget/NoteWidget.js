import { mapActions } from 'vuex';
import dialogPolyfill from 'dialog-polyfill';
import BrandButton from '../../library/BrandButton/BrandButton.vue';

/*
* Свойства:
* status: тип обрабатываемого экземпляра заметки:
* - readonly: не редактируемая заметка, на главной
* - default: новая заметка
* - extended: редактируемая заметка, расширенный функционал
* data: объект данных экземпляра заметки
* - id: уникальный идентификатор заметки
* - title: заголовок заметки
* - items: массив объектов чек-листа
*   - key: случайный уникальный ключ для управления реактивным списком
*   - text: текст одного пункта чек-листа
*   - checked: состояние пункта чек-листа (выбран/не выбран)
*
* Реативные данные:
* - noteText: заголовок заметки
* - stack: массив снимков состояния редактирования полей заметки
* - stackId: указатель текущего примененного состояния
* - noteId: уникальный идентификатор заметки
* - todosList: массив объектов пунктов чек-листа
* - isSaveGranted: флаг возможности сохранения заметки
* - spoilerHeight: высота контейнера пунктов чек-листа для управления видимостью списка на главной
* - isSpoilerExplored: флаг состояния (открыт/ закрыт) контейнера пунктов чек-листа
 */

export default {
  name: 'NoteWidget',
  components: {
    BrandButton,
  },
  props: {
    status: {
      type: String,
      default: 'readonly',
    },
    data: {
      type: Object,
      default: () => ({
        items: [
          {
            key: Math.floor(10000 * Math.random()),
            text: '',
            checked: false,
          },
        ],
      }),
    },
  },
  data() {
    return {
      noteText: '',
      stack: [],
      stackId: -1,
      noteId: typeof this.data.id !== 'undefined' ? this.data.id : this.$store.state.widgets.length,
      noteTitle: this.data.title || '',
      todosList: this.data.items || [],
      isSaveGranted: false,
      spoilerHeight: 0,
      isSpoilerExplored: false,
    };
  },
  created() {
    // добавляем пустой пункт в конец списка чек-листа для возможности добавления новых пнктов
    if (this.status === 'extended') {
      this.todosList.push({
        key: this.genUniqueKey(),
        title: '',
        checked: false,
      });
    }
    // сохраняем первый снимок начального состояния заметки
    this.updateStack();
  },
  mounted() {
    dialogPolyfill.registerDialog(this.$refs.dialog);
    // вычисляем высоту урезанного контейнера для заметки на главной
    if (this.status === 'readonly') {
      this.getSpoilerHeight();
    }
    // this.$refs.dialog.setAttribute('open', null);
    // this.$refs.dialog.close();
  },
  methods: {
    ...mapActions({
      save: 'saveNote',
      delete: 'deleteNote',
    }),
    /*
    ** Обработчик сохранения заметки
     */
    saveItem() {
      // получаем копию массива списка пунктов, удаляем последний управляющий пункт и сохраняем заметку
      const list = JSON.parse(JSON.stringify(this.todosList));
      list.splice(list.length - 1);
      this.save({
        id: this.noteId,
        title: this.noteTitle,
        items: list,
      });
      // обнуляем стэк состояний и переходим на главную
      this.stackId = -1;
      this.stack.splice();
      this.$router.push('/');
    },
    /*
    ** Обработчик отображения элемента checkbox пунктов списка
     */
    getItemStatus(index) {
      if (this.status === 'readonly') {
        return true;
      }
      return index < this.todosList.length - 1;
    },
    /*
    ** Обработчик события blur для пункта списка
     */
    updateItem(e, index) {
      if (this.status !== 'readonly' && index !== this.todosList.length - 1) {
        // если пункт не содержит текста, кдаляем из списка
        if (!e.target.innerText) {
          this.removeItem(index);
        } else if (e.target.innerText !== this.todosList[index].text) { // если текст уникальный, обновляем список и делаем снимок состояния
          // eslint-disable-next-line
          e.target.innerText = this.noteText;
          this.todosList[index].text = this.noteText;
          this.updateStack();
        }
        this.noteText = '';
      }
    },
    /*
    ** Обработчик события input для пункта списка
     */
    handleItem(e, index) {
      // если данные вводятся в управляющий пункт, добавляем пустой объект и текущий пункт из управляющего становится пользовательским
      if (this.todosList.length === index + 1) {
        this.todosList.push({
          key: this.genUniqueKey(),
          text: '',
          checked: false,
        });
      }
      // записываем значение пункта во временную переменную
      this.noteText = e.target.innerText;
      // проверяем возможность записи пункта при каждом событии ввода
      this.getSavePermission(index);
    },
    /*
    ** Обработчик удаления пункта из списка чек-листа
     */
    removeItem(index, update) {
      if (this.status === 'readonly') return;
      this.todosList.splice(index, 1);
      // сохраняем снимок состояния только, если он произведен пользователем
      if (update) this.updateStack();
      // проверяем возможность записи пункта
      this.getSavePermission(index);
    },
    /*
    ** Обработчик элемента checkbox пункта
     */
    checkItem(e, index) {
      if (this.status === 'readonly') return;
      const status = e.target.getAttribute('aria-checked') === 'true';
      e.target.setAttribute('aria-checked', !status ? 'true' : 'false');
      this.todosList[index].checked = !status;
      this.updateStack();
      this.getSavePermission(index);
    },
    /*
    ** Обработчик дейтсвия "Назад"
     */
    stepBefore() {
      this.stackId -= 1;
      this.applyCommit();
    },
    /*
    ** Обработчик дейтсвия "Вперед"
     */
    stepAfter() {
      this.stackId += 1;
      this.applyCommit();
    },
    /*
    ** Обработчик применения сохраненного состояния к заметке
     */
    applyCommit() {
      this.noteTitle = this.stack[this.stackId].title;
      this.todosList.length = 0;
      this.stack[this.stackId].items.forEach((el) => {
        // eslint-disable-next-line
        el.key = this.genUniqueKey();
        this.todosList.push(el);
      });
      this.getSavePermission(-1);
    },
    /*
    ** Обработчик сохранения снимка состояния заметки в стэк состояний
     */
    updateStack() {
      if (this.stackId < this.stack.length - 1) {
        this.stack.splice(this.stackId + 1);
      }
      this.stack.push({
        title: this.noteTitle,
        items: JSON.parse(JSON.stringify(this.todosList)),
      });
      this.stackId += 1;
    },
    /*
    ** Обработчик действия "Отмена"
     */
    cancelItem() {
      // применяем начальное состояние из стэка
      this.stackId = 0;
      this.applyCommit();
      // обнуляем стэк состояний
      this.stack.length = 0;
      // удаляем управляющий пункт списка если заметка в расширенном режиме
      if (this.status === 'extended') {
        this.removeItem(this.todosList.length - 1);
      }
      this.$router.push('/');
    },
    /*
    ** Обработчик подтверждения действия "Удалить"
     */
    showModal() {
      this.$refs.dialog.showModal();
      this.$refs.dialog.classList.remove('state_hidden');
    },
    /*
    ** Обработчик действия "Удалить"
     */
    deleteNote() {
      this.delete(this.noteId);
      this.$refs.dialog.close();
      if (this.$route.name === 'Note') {
        this.$router.push('/');
      }
    },
    /*
    ** Обработчик возможности сохранения заметки
    *   проверяет наличие заголовка и хотя бы одного пункта заметки, при этом
    *   если данные соответствуют начальному снимку состояния заметки, значит никаких уникальных действий
    *   выполненно не было
     */
    getSavePermission(currentIndex) {
      if (this.todosList.length - 1 < currentIndex) return;
      let isIdentical = true;
      if (this.stack[0].items.length === this.todosList.length) {
        this.stack[0].items.forEach((el, index) => {
          if (index < this.stack[0].items.length - 1) {
            const text = currentIndex !== index ? this.todosList[index].text : this.noteText;
            if (el.checked !== this.todosList[index].checked || el.text !== text) {
              isIdentical = false;
            }
          }
        });
      } else {
        isIdentical = !(this.todosList.length > 1);
      }
      if (!this.noteTitle || this.todosList.length < 2) {
        this.isSaveGranted = false;
      } else if (this.status === 'default') {
        this.isSaveGranted = (this.noteTitle !== this.stack[0].title) && !isIdentical;
      } else {
        this.isSaveGranted = (this.noteTitle !== this.stack[0].title) || !isIdentical;
      }
    },
    /*
    ** Функция получения высоты контейнера для первых трех пунктов чек-листа, для отображения на главной
     */
    getSpoilerHeight() {
      const $listItems = Array.from(this.$refs.list.querySelectorAll('li'));
      if ($listItems.length > 3) {
        $listItems.splice(3);
      }
      this.spoilerHeight = $listItems.reduce((acc, cur) => acc + cur.scrollHeight, 0);
      this.$refs.spoiler.style.maxHeight = `${this.spoilerHeight + 48}px`;
    },
    /*
    ** Обработчик состояния (открыт/ закрыт) для контейнера пунктов чек-листа
     */
    exploreList() {
      if (!this.isSpoilerExplored) {
        this.$refs.spoiler.style.maxHeight = `${this.$refs.list.scrollHeight + 48}px`;
      } else {
        this.$refs.spoiler.style.maxHeight = `${this.spoilerHeight + 48}px`;
      }
      this.isSpoilerExplored = !this.isSpoilerExplored;
    },
  },
};
