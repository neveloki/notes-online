import BrandButton from '../../components/library/BrandButton/BrandButton.vue';
import NoteWidget from '../../components/blocks/NoteWidget/NoteWidget.vue';


export default {
  name: 'Home',
  components: {
    BrandButton,
    NoteWidget,
  },
  data() {
    return {
      list: this.$store.state.widgets,
    };
  },
};
